#!/usr/bin/env python3

import sys, os, hashlib, glob, re

def get_pkg_hash_from_Packages(Packages_file, package, version, hash="SHA256"):
    with open(Packages_file, 'r') as Packages:
        package_list = Packages.read().split('\n\n')
    for pkg in package_list:
        if pkg.split('\n')[0] == "Package: "+package:
            for line in pkg.split('\n'):
                if line.startswith('Version:') and line != 'Version: '+version:
                    # Seems the repo contains the wrong version, or several versions
                    # We can't use this one so continue looking
                    break
                elif line.startswith('Filename:') and line.split()[1].startswith('pool'):
                    # The pool folder is a residual from metadata generation. Do not
                    # use a deb found there for hash comparison
                    break
                elif line.startswith(hash):
                    return line.split()[1]

def calc_deb_sha256(deb):
    BUF_SIZE = 65536
    sha256 = hashlib.sha256()
    with open(os.path.join(arch_path, deb), 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha256.update(data)
    return sha256.hexdigest()

def find_Packages(path, arch):
    Packages = glob.glob(os.path.join(path, "*/*/binary-"+arch, "Packages"))
    if len(Packages) > 1:
        print("Error: too many Packages files found")
        print(Packages)
        sys.exit(1)
    return Packages[0]

def get_name_and_version(deb):
    expr = "(.*)_(.*)_(arm|aarch64|x86_64|i686|all).deb"
    match = re.match(expr, deb)
    if not match:
        print("Error: failed to match regex to "+deb)
        sys.exit(1)
    return match.group(1), match.group(2)

if __name__ == '__main__':
    mirror_base = "/storage/termux-mirrors/mirror"
    for user in os.listdir(os.path.join(mirror_base, "dl.bintray.com")):
        for repo in os.listdir(os.path.join(mirror_base, "dl.bintray.com", user)):
            repo_path = os.path.join(mirror_base, "dl.bintray.com", user, repo)
            for arch in os.listdir(repo_path):
                if arch == "dists" or arch == "pool":
                    continue
                print("Entering"+os.path.join(repo_path, arch))
                arch_path = os.path.join(repo_path, arch)
                for deb in os.listdir(arch_path):
                    sha256sum = calc_deb_sha256(os.path.join(arch_path, deb))

                    pkg_name, pkg_version = get_name_and_version(deb)
                    Packages = find_Packages(os.path.join(repo_path, "dists"), arch)
                    packages_sha256sum = get_pkg_hash_from_Packages(Packages, pkg_name, pkg_version)

                    if sha256sum != packages_sha256sum:
                        print("Failed match for "+os.path.join(arch_path, deb).replace(os.path.join(mirror_base, "dl.bintray.com", user)+"/", ""))
                        print("Calculated: "+sha256sum)
                        print("Packages:   "+packages_sha256sum)
                        print("Deleting deb file")
                        os.remove(os.path.join(arch_path, deb))
                        print("")
