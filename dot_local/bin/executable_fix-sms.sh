#!/bin/sh

# Script to fix so that text messages can be received in phosh,
# see https://gitlab.com/postmarketOS/pmaports/-/issues/1001
sudo rc-service modemmanager stop
sleep 1
echo 'AT+CPMS="MT","MT","MT"' | sudo atinout - /dev/modem-at -
sleep 1
sudo rc-service modemmanager start
