#!/bin/sh

set -e

### passwords for keystore
keystore_pw=$(pass dev/keystore)
key_pw=$(pass dev/keystore-termux)

### your keystore file
keystore_file=Grimler.keystore

if [ -z $1 ]; then
    apps="app api float tasker styling boot x11 widget"
else
    apps="$1"
fi

mkdir -p apps

for app in $apps; do
    if [ -d "termux-$app" ] || [ "$(basename $(pwd))" == "termux-$app" ]; then
        if [ -d "termux-$app" ]; then cd termux-$app; fi
        OUTDIR=app/build/outputs/apk/release/
        APK_NAME=app
{{- /* loop from 0 to length of apps.names and print app settings for every $index. */ -}}
{{- range $index, $_ := (index (index . "apps") "names") }}
    elif [ "$app" == "{{ (index (index $.apps "names") $index) }}" ]; then
        cd $app
        OUTDIR={{ (index (index $.apps "outdir") $index) }}
        APK_NAME={{ (index (index $.apps "apk_name") $index) }}
{{- end }}
    else
        echo "$app is not a recognized app"
        exit 1
    fi

    branch=$(git rev-parse --abbrev-ref HEAD)
    # git pull

    ./gradlew clean
    # Build with gradle:
    ./gradlew build
    if [ ! $? -ne 0 ]; then
        # Signing the app:
        jarsigner -sigalg SHA1withRSA -digestalg SHA1 \
                  -keystore ~/.android/${keystore_file} \
                  -storepass ${keystore_pw} \
                  -keypass ${key_pw} \
                  $OUTDIR/$APK_NAME-release-unsigned.apk termux_app

        # Align the app (not strictly necessary)
        zipalign -v -p 4 $OUTDIR/$APK_NAME-release-{unsigned,aligned}.apk

        # Move finished apk to folder "apps"
        cp $OUTDIR/$APK_NAME-release-aligned.apk ../apps/$app-$branch.apk
    fi
    cd ..
done
