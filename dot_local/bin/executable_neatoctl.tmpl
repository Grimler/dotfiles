#!/usr/bin/python3

# Uses https://github.com/stianaske/pybotvac to control our vacuum cleaner

import sys, subprocess, os

from pybotvac import Robot
from pybotvac import Account

if len(sys.argv) < 2:
    print("Error: need an argument out of: start, stop, status, map.")
    sys.exit(1)

robot = Robot("{{ .neato.serial }}", "{{ .neato.secret }}", "Melker")

if sys.argv[1] == "start":
    robot.start_cleaning()
elif sys.argv[1] == "stop":
    robot.stop_cleaning()
elif sys.argv[1] == "status":
    resp = robot.get_robot_state()
    print(resp.text)

elif sys.argv[1] == "map":
    pw_file = os.path.expanduser("~")+"/.password-store/web/neato.gpg"
    password = subprocess.check_output(["gpg", "-d", "-q", pw_file], encoding="utf8").strip('\n')
    acc = Account("{{ .email.outlook }}", password)
    download_link = acc.maps["{{ .neato.serial }}"]['maps'][0]['url']

    resp = acc.get_map_image(download_link)
    with open(os.path.expanduser("~")+"/neato_map.png", 'wb') as f:
        for line in resp:
            f.write(line)
    print("latest map written to '~/neato_map.png'.")
else:
    print("Error: unknown argument")
    sys.exit(1)
