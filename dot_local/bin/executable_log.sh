#!/bin/sh

# Run dmesg and logread in a tmux window, and tee to log files.
mkdir -p logs
cd logs
name=$(ls $(date +%Y%m%d)_*.dmesg | sort -r | head -n 1)
if [ ! -z "$name" ]; then
        num=$(echo "$name" | grep -o -E '(\d*).dmesg' | grep -o -E '(\d*)')
        new_num=$((num+1))
        new_name="$(date +%Y%m%d)_${new_num}"
else
        new_name="$(date +%Y%m%d)_1"
fi

tmux new-session -d -s log "dmesg --follow | tee ${new_name}.dmesg";
tmux split-window;
tmux send "logread -F | tee ${new_name}.logread" ENTER;
tmux a;
