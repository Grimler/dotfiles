# Dotfile managing systems

This is my third attempt at finding a dotfile handling system I like. 

I started with a homemade git+Makefile solution before investigation
the available options out there.

In the next iteration I used
[dotdrop](https://github.com/deadc0de6/dotdrop) and it worked pretty
well. However, the method of passing arguments and secrets to
templates, through environmental variables, got annoying. It was not
possible to pass any sort of arrays and then loop over them with
something like (pseudo-code):

```
{@@ for name, key in contact, gpg_keys @@}
{{@@ name @@}} {{@@ key @@}}
{@@ end @@}
```

so I ended up having

```
{{@@ env['name_1'] @@}} {{@@ env['key_1'] @@}}
{{@@ env['name_2'] @@}} {{@@ env['key_2'] @@}}
{{@@ env['name_3'] @@}} {{@@ env['key_3'] @@}}
...
```
and so forth. It did however have other nice features, like the
ability to check if a file/folder exists.
I also did not like that it was not possible to have if-statements in
the file with environmental variable.

chezmoi on the other hand seems very promising so far, it supports
arrays and the way of supplying variables in a .toml config file is
neat. Lets hope this is my final dotfile handling system.

# Variables in ~/.config/chezmoi/chezmoi.toml

I keep my chezmoi.toml encrypted in another private repository. It
contains lots of variables, some that could be public (like the public
part of my ssh keys), some secrets like token keys, and some private
information (not really secrets but not really information I want to
share with the world either). Having these in another repo also means
that I do not clutter the git history of this repo with commits like
"Update public ssh key".

Here is the structure of my chezmoi.toml to help you understand
chezmoi and these dotfiles:
```
[data]
  name = "<>"

  phone = "<>"
  work_phone = "<>"

  gpg_fingerprint = "<>"
  gpg_work_fingerprint = "<>"

  [data.email]
    main = "<>"
    work = "<>"
    gmail = "<>"
    outlook = "<>"
    mailing_lists = "<>"

    lists = "folders to sync"

  [data.ssh]
    authorized_keys = [
      "ssh-rsa <public key>",
    ]

    hosts = [
      "<host>",
    ]
    ips = [
      "<192.168.0.1>",
    ]
    users = [
      "<username>",
    ]
    ports = [
      "<port>",
    ]

  [data.gpg]
    mail_addresses = [
      "<email>",
    ]
    fingerprints = [
      "<gpg fingerprint>",
    ]

  [data.termux]
    prefix = "/data/data/com.termux/files/usr"

  [data.work]
    workspace = "<slack slack>"
    channel = "<slack channel>"

  [data.vpn]
    VPN_NAME = "<vpn to start with systemctl>"

  [data.ip_addresses]
    home = "<>"
    work = "<>"
    public = "<>"
    pi = "<>"

  [data.ports]
    pi = "<>"
    grimlerstat = "<>"
    grimlerwork = "<>"

  [data.mac_addresses]
    grimlerstat = "<>"
    work = "<>"

  [data.device_addresses]
    tablet = "<>"
    phone = "<>"
    old = "<>"

  [data.onion_addresses]
    pi = "<>"

  [data.calendar]
    address = "<>"

  [data.sl_api]
    platsuppslag = "<>"
    reseplanerare3 = "<>"
    reseplanerare31 = "<>"
    realtidsinformation4 = "<>"

  [data.tokens]
    GITTER = "<>"
    SLACK = "<>"
    GITHUB_FORGE = "<>"
    GITLAB_FORGE = "<>"

  [data.neato]
    SECRET = "<>"
    SERIAL = "<>"

  [data.irssi]
    chatnets = [
      "libera",
    ]
    channels = [
      "#termux",
    ]

  [data.podcasts]
    names = [
      "<>",
    ]

    urls = [
      "<>",
    ]

  [data.apps]
    names = [
      "Mycroft-Android",
    ]

    url = [
      "git@github.com:MycroftAI/Mycroft-Android",
    ]

    outdir = [
      "mobile/build/outputs/apk/release/",
    ]

    apk_name = [
      "mobile",
    ]

  [data.ncgroceries]
    shoppinglist = "Shopping list"

  [data.rss_feeds]
    feeds = [
      "https://rss_feed_url",
    ]
```
